FROM 192.168.1.55/demo/php-fpm:5.4

ADD init.sh /init.sh

CMD ["/init.sh", "/usr/bin/supervisord", "-n", "-c", "/etc/supervisord.conf"]
